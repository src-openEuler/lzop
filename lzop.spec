Name:		lzop
Summary:	lzop is a file compressor very similar to gzip.
Version:	1.04
Release:	3
License:	GPLv2+
URL:		https://www.lzop.org/
Source:		https://www.lzop.org/download/%{name}-%{version}.tar.gz
Patch1:         lzop-Add-sw64-architecture.patch
BuildRequires:	gcc lzo-devel

%description
lzop is a file compressor which is very similar to gzip. lzop uses
the LZO data compression library for compression services, and its
main advantages over gzip are much higher compression and decompression
speed (at the cost of some compression ratio).

%package_help

%prep
%setup -q
%patch1 -p1

%build
%configure
%make_build

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p' install

%files
%{!?_licensedir:%global license %%doc}
%license COPYING AUTHORS
%doc NEWS README THANKS
%{_bindir}/*

%files help
%{_pkgdocdir}
%{_mandir}/man?/%{name}.*

%changelog
* Mon Nov 14 2022 wuzx<wuzx1226@qq.com> - 1.04-3
- Add sw64 architecture

* Tue Oct 25 2022 yanglongkang <yanglongkang@h-partners.com> - 1.04-2
- rebuild for next release

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.04-1
- Package init
